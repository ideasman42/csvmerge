
#########
CSV Merge
#########

Utility to merge multiple CSV files.

This was written to update transaction data from banks, adding only new transactions.

This supports limiting the columns used for comparison (specifically  ``--compare-end``),
so you can add extra columns at the end which will be kept without interfering with the merge.


Command Line Arguments
======================

.. BEGIN HELP TEXT

Output of ``csvmerge --help``

usage::

       csvmerge [-h] --input INPUT_FILEPATH [INPUT_FILEPATH ...] --output
                OUTPUT_FILEPATH [--input-dialect DIALECT]
                [--output-dialect DIALECT] [--compare-begin COLUMN_INDEX]
                [--compare-end COLUMN_INDEX] [--method OVERLAP] [--verbose]

Take multiple CSV files and output a new file, de-duplicating contents.

Example:

   csvmerge --output my_output.csv --input first.csv second.csv

optional arguments:
  -h, --help            show this help message and exit
  --input INPUT_FILEPATH
                        The first file is the base CSV file, all other input files are merged into this one.
  --output OUTPUT_FILEPATH
                        The merged output to write to. Note that this may include a file from the input without causing problems.
  --input-dialect DIALECT
                        Input dialect used when parsing CSV files in ['excel', 'excel-tab', 'unix'].
  --output-dialect DIALECT
                        Output dialect used when writing CSV files in ['excel', 'excel-tab', 'unix'].
  --compare-begin COLUMN_INDEX
                        Start comparing at this column index (where zero is the first item, inclusive).
                        When omitted, start at the first.
  --compare-end COLUMN_INDEX
                        End comparing at this column index (where zero is the first item, not inclusive).
                        When omitted, end at the last.
  --method OVERLAP      Method to merge files.

                        - ``OVERLAP`` detects overlap at either ends of the files and merges, removing any overlap.
                          Unique merges all rows as long as they do not match an existing row.
                        - ``UNIQUE`` only merges rows which have no exact match.
                          This uses a faster method compared to ``OVERLAP`` and each row is guarantee to be unique.

                          Do not use this method if multiple, identical transactions are a possibility
                          (can happen for CVS files without a balance column).
  --verbose             Print extra information.

.. END HELP TEXT
